import logo from "./logo.svg";
import "./App.css";
import Ex_FormSinhVien from "./Ex_FormSinhVien/Ex_FormSinhVien";

function App() {
  return (
    <div className="App">
      <Ex_FormSinhVien />
    </div>
  );
}

export default App;
