import React, { Component } from "react";
import UserForm from "./UserForm";
import UserTable from "./UserTable";

export default class Ex_Form extends Component {
  state = {
    userList: [
      {
        id: 1,
        name: "Alice",
        password: "alice!@#",
        sdt: "0999222333",
        email: "alice@gmail.com",
      },
    ],
    userEdited: null,
  };
  handleAddNewUser = (newUser) => {
    let cloneArr = [...this.state.userList, newUser];
    this.setState({ userList: cloneArr });
  };
  handleRemoveUser = (userId) => {
    let index = this.state.userList.findIndex((item) => {
      return item.id == userId;
    });
    if (index != -1) {
      let cloneArr = [...this.state.userList];
      cloneArr.splice(index, 1);
      this.setState({
        userList: cloneArr,
      });
    }
  };
  handleEditUser = (value) => {
    this.setState({
      userEdited: value,
    });
  };
  componentWillReceiveProps(nextProps) {
    console.log("nextProps: ", nextProps);
    if (nextProps.userEdited != null) {
      this.setState({
        user: nextProps.userEdited,
      });
    }
  }
  render() {
    return (
      <div className="container py-5">
        <UserForm
          handleEditUser={this.state.userEdited}
          handleAddNewUser={this.handleAddNewUser}
        />
        <UserTable
          handleEditUser={this.handleEditUser}
          handleRemoveUser={this.handleRemoveUser}
          userList={this.state.userList}
        />
      </div>
    );
  }
}
