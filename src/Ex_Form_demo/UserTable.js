import React, { Component } from "react";

export default class UserTable extends Component {
  renderUserTable = () => {
    return this.props.userList.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.sdt}</td>
          <td>{item.email}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemoveUser(item.id);
              }}
              className="btn btn-danger mr-2"
            >
              Delete
            </button>
            <button
              onClick={() => {
                this.props.handleEditUser(item);
              }}
              className="btn btn-warning"
            >
              Edit
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>SDT</th>
            <th>Email</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{this.renderUserTable()}</tbody>
      </table>
    );
  }
}
