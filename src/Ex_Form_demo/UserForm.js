import React, { Component } from "react";
import { nanoid } from "nanoid";
export default class UserForm extends Component {
  state = {
    user: {
      name: "",
      password: "",
      sdt: "",
      email: "",
    },
  };
  handleGetUserForm = (e) => {
    let { value, name: key } = e.target;
    //name:key ~ đổi tên name thành key
    let cloneUser = { ...this.state.user };
    cloneUser[key] = value;
    this.setState({ user: cloneUser }, () => {
      console.log(this.state.user);
    });
  };
  handleSubmitNewUser = () => {
    let newUser = { ...this.state.user };
    newUser.id = nanoid(5);
    this.props.handleAddNewUser(newUser);
  };
  render() {
    return (
      <div>
        <div className="form-group">
          <input
            onChange={this.handleGetUserForm}
            value={this.state.user.name}
            type="text"
            className="form-control"
            name="name"
            placeholder="Username"
          />
        </div>
        <div className="form-group">
          <input
            onChange={this.handleGetUserForm}
            value={this.state.user.password}
            type="text"
            className="form-control"
            name="password"
            placeholder="Password"
          />
        </div>
        <div className="form-group">
          <input
            onChange={this.handleGetUserForm}
            value={this.state.user.sdt}
            type="text"
            className="form-control"
            name="sdt"
            placeholder="SĐT"
          />
        </div>
        <div className="form-group">
          <input
            onChange={this.handleGetUserForm}
            value={this.state.user.email}
            type="text"
            className="form-control"
            name="email"
            placeholder="Email"
          />
        </div>
        <button onClick={this.handleSubmitNewUser} className="btn btn-success">
          Thêm sinh viên
        </button>
      </div>
    );
  }
}
