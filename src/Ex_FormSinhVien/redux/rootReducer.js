import { combineReducers } from "redux";
import { formReducer } from "./formReducer";
export const rootReducer_form = combineReducers({
  formReducer,
});
