export const initialState = {
  svListArr: [
    {
      id: 1,
      hoTen: "Guitar",
      phone: "0364677779",
      email: "dangnguyenguitar@gmail.com",
    },
    {
      id: 2,
      hoTen: "Nguyen Van A",
      phone: "0752712213",
      email: "nguyenvana@gmail.com",
    },
  ],
};
export const formReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "THEM_SV": {
      let cloneSvArr = [...state.svListArr, payload];
      return { ...state, svListArr: cloneSvArr };
    }
    default:
      return state;
  }
};
