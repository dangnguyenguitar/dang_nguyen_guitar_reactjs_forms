import React, { Component } from "react";
import SvInput from "./SvInput";
import SvList from "./SvList";

export default class Ex_FormSinhVien extends Component {
  render() {
    return (
      <div>
        <SvInput />
        <SvList />
      </div>
    );
  }
}
