import React, { Component } from "react";
import { connect } from "react-redux";

class SvList extends Component {
  renderSvList = () => {
    return this.props.danhSachSv.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.hoTen}</td>
          <td>{item.phone}</td>
          <td>{item.email}</td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table container my-3">
        <thead className="bg-dark text-white">
          <tr>
            <th>Mã sv</th>
            <th>Họ tên</th>
            <th>Số điện thoại</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>{this.renderSvList()}</tbody>
      </table>
    );
  }
}
let mapStatetoProps = (state) => {
  return { danhSachSv: state.formReducer.svListArr };
};
export default connect(mapStatetoProps)(SvList);
