import React, { Component } from "react";
import { connect } from "react-redux";

class SvInput extends Component {
  state = {
    student: {
      id: "",
      hoTen: "",
      phone: "",
      email: "",
    },
  };
  handleGetStudentValue = (e) => {
    // console.log(e.target.name);
    let { value, name } = e.target;
    let cloneStudent = { ...this.state.student };
    cloneStudent[name] = value;
    this.setState(
      {
        student: cloneStudent,
      },
      () => {
        console.log(this.state.student);
      }
    );
  };
  render() {
    return (
      <div className="container text-left">
        <h3 className="bg-dark text-white p-3">Thông tin sinh viên</h3>
        <div className="row">
          <div className="form-group col-6">
            <input
              onChange={this.handleGetStudentValue}
              value={this.state.student.id}
              type="text"
              className="form-control"
              name="id"
              placeholder="Mã sinh viên"
            />
          </div>
          <div className="form-group col-6">
            <input
              onChange={this.handleGetStudentValue}
              value={this.state.student.hoTen}
              type="text"
              className="form-control"
              name="hoTen"
              placeholder="Họ và tên"
            />
          </div>
          <div className="form-group col-6">
            <input
              onChange={this.handleGetStudentValue}
              value={this.state.student.phone}
              type="text"
              className="form-control"
              name="phone"
              placeholder="Số điện thoại"
            />
          </div>
          <div className="form-group col-6">
            <input
              onChange={this.handleGetStudentValue}
              value={this.state.student.email}
              type="text"
              className="form-control"
              name="email"
              placeholder="Email"
            />
          </div>
        </div>
        <button
          onClick={() => {
            this.handleAddNewUser(this.state.student);
          }}
          className="btn btn-success"
        >
          Thêm sinh viên
        </button>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleAddNewUser: (sinhVien) => {
      dispatch({
        type: "THEM_SV",
        payload: sinhVien,
      });
    },
  };
};
export default connect(null, mapDispatchToProps)(SvInput);
